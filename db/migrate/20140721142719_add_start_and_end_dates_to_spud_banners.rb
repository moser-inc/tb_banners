class AddStartAndEndDatesToSpudBanners < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_banners, :start_date, :date
    add_column :spud_banners, :end_date, :date
  end
end
