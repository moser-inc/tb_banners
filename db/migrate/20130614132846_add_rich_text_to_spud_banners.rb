class AddRichTextToSpudBanners < ActiveRecord::Migration[4.2]
  def change
    add_column :spud_banner_sets, :has_rich_text, :boolean, :default => false
    add_column :spud_banners, :rich_text, :text
  end
end
