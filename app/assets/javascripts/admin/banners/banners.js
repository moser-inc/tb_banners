// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

(function(){

var bannerEditId = false;

tb.banners = {

  // Index Page
  /////////////
  init: function(){
    $('.admin-banner-set-item-add').on('click', clickedAddNewBanner);
    $('.admin-banner-set-items-container').on('click', '.admin-banner-set-item-edit', clickedEditBanner);
    $('.admin-banner-set-items-container').on('click', '.admin-banner-set-item-delete', clickedDeleteBanner);
    $('.modal-body').on('submit', '.admin-banner-form', submittedBannerForm);
    Sortable.create(document.querySelector('.admin-banner-set-items-container'), {
      onUpdate: sortedBanners,
      dragClass: 'banner-dragging',
      handle: '.glyphicon-sort'
    });
  }
};

var clickedAddNewBanner = function(e){
    e.preventDefault();
    bannerEditId = false;
    spud.admin.editor.unload();
    $.ajax({
      url: $(this).attr('href'),
      dataType: 'html',
      success: function(html, textStatus, jqXHR){
        spud.admin.modal.displayWithOptions({
          title: 'Upload Banner',
          html: html
        });
        initForm();
      }
    });
  };

var clickedEditBanner = function(e){
  e.preventDefault();
  bannerEditId = parseInt($(this).parents('.admin-banner-set-item').attr('data-id'), 10);
  spud.admin.editor.unload();
  $.ajax({
    url: $(this).attr('href'),
    dataType: 'html',
    success: function(html, textStatus, jqXHR){
      spud.admin.modal.displayWithOptions({
        title: 'Edit Banner',
        html: html
      });
      initForm();
    }
  });
};

var clickedDeleteBanner = function(e){
  e.preventDefault();
  $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': Rails.csrfToken() }
  });
  var el = $(this);
  $.ajax({
    url: el.attr('href'),
    type: 'post',
    data: {'_method':'delete'},
    complete: function(jqXHR, textStatus){
      var parent = el.parents('.admin-banner-set-item');
      parent.fadeOut(200, function(){
        parent.remove();
      });
      if(textStatus != 'success'){
        console.warn('Something went wrong:', jqXHR);
      }
    }
  });
};

var submittedBannerForm = function(e){
  e.preventDefault();

  var form = $(this);
  var fd = new FormData();

  $('input[type=text], input[type=hidden], select').each(function(index, element){
    var input = $(element);
    var name = input.attr('name');
    var value = input.val();
    fd.append(name, value);
  });

  var editor = tinymce.get('admin-banner-rich-text');
  if(editor){
    fd.append('spud_banner[rich_text]', editor.getContent());
  }
  else{
    fd.append('spud_banner[rich_text]', '');
  }

  var file = form.find('#spud_banner_banner')[0].files[0];
  if(file){
    fd.append('spud_banner[banner]', file);
    $('.admin-banner-upload-progress').show();
  }

  var xhr = new XMLHttpRequest();
  xhr.upload.addEventListener('progress', onFileUploadProgress);
  xhr.addEventListener('load', onFileUploadComplete);
  xhr.addEventListener('error', onFileUploadError);
  xhr.addEventListener('abort', onFileUploadAbort);
  xhr.open('POST', form.attr('action'));
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.send(fd);
  return false;
};

var onFileUploadProgress = function(e){
  var percent = Math.round(e.loaded * 100 / e.total);
  var progress = $('.admin-banner-upload-progress');
  progress.find('.bar').css({width: percent + '%'});
  if(percent == 100){
    progress.addClass('progress-success');
  }
};

var onFileUploadComplete = function(e){
  switch(this.status){
    case 200:
      onUploadComplete(e.target.response);
      break;
    case 422:
      onUploadError(e.target.response);
      break;
    default:
      window.alert("Whoops! Something has gone wrong.");
  }
};

var onFileUploadError = function(e){

};

var onFileUploadAbort = function(e){

};

var onUploadComplete = function(html){
  if(bannerEditId){
    var item = $('.admin-banner-set-item[data-id="'+bannerEditId+'"]');
    item.replaceWith(html);
  }
  else{
    $('.admin-banner-set-items-container').append(html);
  }
  spud.admin.modal.hide();
};

var onUploadError = function(html){
  $('.admin-banner-form').replaceWith(html);
  initForm();
};

var sortedBanners = function(e, ui){
  var ids = [];
  $('.admin-banner-set-item').each(function(){
    ids.push($(this).attr('data-id'));
  });
  $.ajax({
    headers: { 'X-CSRF-TOKEN': Rails.csrfToken() },
    url: '/admin/banners/sort',
    type: 'post',
    data: { spud_banner_ids: ids, _method: 'put' }
  });
};

var initForm = function(){
  var richText = $('#admin-banner-rich-text');
  if(richText){
    if(typeof tinymce != "undefined"){
      tinymce.EditorManager.execCommand('mceRemoveEditor', true, 'admin-banner-rich-text');
    }
    spud.admin.editor.init({
      selector: '#admin-banner-rich-text',
      buttons: ['bold','italic','underline','formatselect','|','bullist','numlist','|','link','unlink','anchor', '|', 'code'],
      height: 300,
      width: 500
    });
  }
};

})();
