// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

(function(){

var bannerSetEditId = false;

tb.banner_sets = {
  init: function(){
    $('.spud_banner_sets_add_new').on('click', clickedAddNewBannerSet);
    $('.admin-banner-sets-list').on('click', '.admin-banner-set-edit', clickedEditBannerSet);
    $('.admin-banner-sets-list').on('click', '.admin-banner-set-delete', clickedDeleteBannerSet);
    $('.modal-body').on('submit', '.admin-banner-set-form', submittedBannerSetForm);
  }
};

var clickedAddNewBannerSet = function(e){
  e.preventDefault();
  bannerSetEditId = false;
  $.ajax({
    url: $(this).attr('href'),
    dataType: 'html',
    success: function(html, textStatus, jqXHR){
      spud.admin.modal.displayWithOptions({
        title: 'New Banner Set',
        html: html
      });
    }
  });
};

var clickedEditBannerSet = function(e){
  e.preventDefault();
  bannerSetEditId = parseInt($(this).parents('tr').attr('data-id'), 10);
  $.ajax({
    url: $(this).attr('href'),
    dataType: 'html',
    success: function(html, textStatus, jqXHR){
      spud.admin.modal.displayWithOptions({
        title: 'Edit Banner Set',
        html: html
      });
    }
  });
};

var clickedDeleteBannerSet = function(e){
  e.preventDefault();
  $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': Rails.csrfToken() }
  });
  if(window.confirm('Are you sure?')){
    var el = $(this);
    $.ajax({
      url: el.attr('href'),
      type: 'post',
      data: {'_method':'delete'},
      complete: function(jqXHR, textStatus){
        var parent = el.parents('tr');
        parent.fadeOut(200, function(){
          parent.remove();
        });
        if(textStatus != 'success'){
          console.warn('Something went wrong:', jqXHR);
        }
      }
    });
  }
};

var submittedBannerSetForm = function(e){
  e.preventDefault();
  var form = $(this);
  $.ajax({
    url: form.attr('action'),
    data: form.serialize(),
    type: 'post',
    dataType: 'html',
    success: savedBannerSetSuccess,
    error: savedBannerSetError
  });
};

var savedBannerSetSuccess = function(html){
  if(bannerSetEditId){
    var item = $('.admin-banner-sets-list-item[data-id="'+bannerSetEditId+'"]');
    item.replaceWith(html);
  }
  else{
    $('.admin-banner-sets-list').append(html);
  }
  spud.admin.modal.hide();
};

var savedBannerSetError = function(jqXHR, textStatus, errorThrown){
  if(jqXHR.status == 422){
    var html = jqXHR.responseText;
    $('.admin-banner-set-form').replaceWith(html);
  }
  else{
    if(window.console){
      console.error('Oh Snap:', arguments);
    }
  }
};

})();
