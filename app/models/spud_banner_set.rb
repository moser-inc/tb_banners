class SpudBannerSet < ActiveRecord::Base
  has_many :banners, -> { order(sort_order: :asc) },
    class_name: SpudBanner.to_s, dependent: :destroy, inverse_of: :owner

  validates :name, presence: true
  validates :name, uniqueness: true
  validates :width, :height, numericality: true

  after_update :check_for_dimension_change

  def self.find_with_identifier(identifier)
    banner_set = if identifier.class == String
                   SpudBannerSet.find_by(name: identifier.strip)
                 elsif identifier.class == Symbol
                   SpudBannerSet.find_by(name: identifier.to_s.titleize)
                 else
                   SpudBannerSet.find(identifier)
                 end
    return banner_set
  end

  def reprocess_banners!
    banners.each do |banner|
      banner.banner.reprocess!
    end
  end

  def set_name
    return name
  end

  private

  def check_for_dimension_change
    reprocess_banners! if saved_change_to_height? || saved_change_to_width? || saved_change_to_cropped?
  end

end
