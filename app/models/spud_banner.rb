class SpudBanner < ActiveRecord::Base
  belongs_to :owner,
    class_name: SpudBannerSet.to_s,
    foreign_key: :spud_banner_set_id,
    inverse_of: :banners,
    touch: true

  scope :active, lambda {
    t = Time.zone.today
    start_date = arel_table[:start_date]
    end_date = arel_table[:end_date]

    where(
      start_date.eq(nil).or(start_date.lteq(t)).and(
        end_date.eq(nil).or(end_date.gteq(t))
      )
    )
  }

  has_attached_file :banner,
    styles: ->(attachment) { attachment.instance.dynamic_styles },
    convert_options: {
      tiny: '-strip -density 72x72',
      banner: '-strip -density 72x72'
    },
    storage: Spud::Banners.paperclip_storage,
    s3_credentials: Spud::Banners.s3_credentials,
    s3_protocol: Spud::Banners.s3_protocol,
    url: Spud::Banners.storage_url,
    path: Spud::Banners.storage_path

  validates_attachment :banner,
    presence: true,
    content_type: { content_type: ['image/jpg', 'image/jpeg', 'image/png'] }

  def dynamic_styles
    styles = {
      tiny: '150x150'
    }
    owner_style = nil
    if owner
      owner_style = "#{owner.width}x#{owner.height}"
      owner_style += '#' if owner.cropped
      styles[:banner] = owner_style
    end
    return styles
  end

  def set_name
    return owner.name
  end

  def expired?
    return end_date.present? && end_date < Time.zone.today
  end

end
