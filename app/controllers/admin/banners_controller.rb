class Admin::BannersController < Admin::ApplicationController

  before_action :load_set, only: [:new, :create]
  before_action :load_record, only: [:edit, :update, :destroy]
  respond_to :html
  layout false

  def new
    @banner = @banner_set.banners.new(start_date: Time.zone.today)
    respond_with @banner
  end

  def create
    @banner = @banner_set.banners.build
    @banner.attributes = banner_params

    last_banner = SpudBanner.select(:sort_order)
                            .where(spud_banner_set_id: @banner_set.id)
                            .order(sort_order: :desc).first
    @banner.sort_order = last_banner.sort_order + 1 if last_banner

    if @banner.save
      flash.now[:notice] = 'Banner created successfully'
      render 'show'
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def edit
    respond_with @banner
  end

  def update
    if @banner.update(banner_params)
      flash.now[:notice] = 'Banner created successfully'
      render 'show'
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def destroy
    @banner.destroy
    head :ok
  end

  # rubocop:disable Rails/SkipsModelValidations
  def sort
    banner_ids = params[:spud_banner_ids]
    banners = SpudBanner.where(id: banner_ids).to_a
    SpudBanner.transaction do
      banner_ids.each_with_index do |id, index|
        banner = banners.select { |b| b.id == id.to_i }.first
        banner.update_column(:sort_order, index)
      end
      banners.last.owner.touch
    end
    head :ok
  end
  # rubocop:enable Rails/SkipsModelValidations

  private

  def load_set
    @banner_set = SpudBannerSet.find_by!(id: params[:banner_set_id])
  end

  def load_record
    @banner = SpudBanner.find_by!(id: params[:id])
  end

  def banner_params
    params.require(:spud_banner).permit(
      :banner, :link_to, :link_target, :title, :alt, :sort_order, :rich_text, :start_date, :end_date
    )
  end

end
