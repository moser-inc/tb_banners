class Admin::BannerSetsController < Admin::ApplicationController

  before_action :load_record, only: [:show, :edit, :update, :destroy]
  respond_to :html
  belongs_to_app :banner_sets
  add_breadcrumb 'Banner Sets', :admin_banner_sets_path
  layout false

  def index
    @banner_sets = SpudBannerSet.all
    respond_with @banner_sets, layout: 'admin/detail'
  end

  def show
    respond_with @banner_set, layout: 'admin/detail'
  end

  def new
    @banner_set = SpudBannerSet.new
    respond_with @banner_set
  end

  def create
    @banner_set = SpudBannerSet.new(banner_set_params)
    if @banner_set.save
      flash.now[:notice] = 'Banner Set created successfully'
      render 'create'
    else
      render 'new', status: :unprocessable_entity
    end
  end

  def edit
    respond_with @banner_set
  end

  def update
    if @banner_set.update(banner_set_params)
      flash.now[:notice] = 'Banner Set updated successfully'
      render 'create'
    else
      render 'edit', status: :unprocessable_entity
    end
  end

  def destroy
    flash.now[:notice] = 'Banner Set deleted successfully' if @banner_set.destroy
    head :ok
  end

  private

  def load_record
    @banner_set = SpudBannerSet.find_by!(id: params[:id])
  end

  def banner_set_params
    params.require(:spud_banner_set).permit(:cropped, :height, :name, :short_name, :width, :has_rich_text)
  end

end
