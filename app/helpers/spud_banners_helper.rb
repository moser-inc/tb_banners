module SpudBannersHelper
  # rubocop:disable  Metrics/MethodLength, Metrics/AbcSize

  # Returns banner html for a given identifier
  #
  # * set_or_identifier: A reference to a SpudBannerSet,
  #   or the name of a known banner set passed as a symbol (ie, :home)
  # * options:
  #   - limit: Max number of banners to return
  #   - background_image: Pass true to return the banners as CSS background-image properties rather than <img/> tags
  #
  def spud_banners_for_set(set_or_identifier, options = {})
    banner_set = if set_or_identifier.is_a?(SpudBannerSet)
                   set_or_identifier
                 else
                   SpudBannerSet.find_with_identifier(set_or_identifier)
                 end

    return '' if banner_set.blank?

    spud_banners_set_tag(banner_set, options)
  end

  # Returns banner html for a given banner_set
  #
  # * banner_set: A reference to a SpudBannerSet
  # * options:
  #   - limit: Max number of banners to return
  #   - background_image: Pass true to return the banners as CSS background-image properties rather than <img/> tags
  #
  def spud_banners_set_tag(banner_set, options = {})
    background_image = options.delete(:background_image)
    limit = options.delete(:limit) || false
    banners_query = banner_set.banners.active.limit(limit)
    if block_given?
      banners_query.each do |banner|
        yield(banner)
      end
    else
      content_tag(:div, :class => "spud-banner-set #{options[:wrapper_class]}", 'data-id' => banner_set.id) do
        banners_query.map do |banner|
          if background_image
            concat(content_tag(:div,
              class: "spud-banner #{options[:banner_class]}",
              style: "background-image:url('#{banner.banner.url(:banner)}');",
              'data-id' => banner.id) do
                content_tag :div, raw(banner.rich_text), class: 'spud-banner-rich-text' if banner.rich_text
              end)
          else
            concat(content_tag(:div, :class => "spud-banner #{options[:banner_class]}", 'data-id' => banner.id) do
              spud_banner_tag(banner, options)
            end)
          end
        end
      end
    end
  end

  # Returns an HTML tag for a single banner
  # May return either an <img/> or an <a/> depending on the banner
  #
  def spud_banner_tag(banner, _options = {})
    if banner.link_to.blank?
      spud_banner_image_tag(banner)
    else
      link_to(banner.link_to, target: banner.link_target) do
        spud_banner_image_tag(banner)
      end
    end
  end

  # Returns an HTML <img/> tag for a single banner
  #
  def spud_banner_image_tag(banner)
    image_tag(banner.banner.url(:banner), alt: banner.alt, title: banner.title)
  end

  # rubocop:enable  Metrics/MethodLength, Metrics/AbcSize
end
