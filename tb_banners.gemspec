$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'spud_banners/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'tb_banners'
  s.version     = Spud::Banners::VERSION
  s.authors     = ['Moser Consulting']
  s.email       = ['greg.woods@moserit.com.com']
  s.homepage    = 'http://bitbucket.org/moser-inc/tb_banners'
  s.summary     = 'Twice Baked Banners'
  s.description = 'Banner Management engine for Twice Baked'

  s.files = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'README.markdown']
  s.test_files = Dir.glob('spec/**/*').reject { |f| f.match(%r{^spec\/dummy\/(log|tmp)}) }

  s.add_dependency 'paperclip', '>= 0'
  s.add_dependency 'rails', '>= 5.0.0.1'
  s.add_dependency 'tb_core', '>= 1.4.4'

  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_bot_rails'
  s.add_development_dependency 'mysql2'
  s.add_development_dependency 'rails-controller-testing'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'simplecov'
end
