# Twice Baked Banners

TB Banners is an engine for creating and managing rotating banner sets, designed for use with [Twice Baked][tb].

## Installation/Usage

1. In your Gemfile add the following

		gem 'tb_banners'

2. Run bundle install
3. Copy in database migrations to your new rails project

		bundle exec rake railties:install:migrations
		rake db:migrate

4. Run a rails server instance and point your browser to `/admin`

## Configuration

TB Banners accepts the following configuration options:

```ruby
Spud::Banners.configure do |config|
	config.paperclip_storage = :filesystem #use :s3 to use s3 storage (aws gem required)
	config.s3_credentials = "#{Rails.root}/config/s3.yml"
	config.s3_protocol = 'https'
	config.storage_path = ":rails_root/public/system/spud_banners/:id/:style/:basename.:extension"
	config.storage_url = "/system/spud_banners/:id/:style/:basename.:extension"
end
```

## Creating a Banner Set

Banner Sets are created in the admin dashboard. When creating a Set, you have the following options:

- **Name:** The unique identifier used to render this set
- **Width:** Maximum width of uploaded banners
- **Height:** Maximum height of uploaded banners
- **Cropped:** Whether or not banners should be cropped to fit into the above Width/Height constraints

## Managing Banners

Once a Set has been created, you can begin adding banners to it. Banners contain optional `link_to`, `link_target`, `title`, and `alt` options. Banners are also sortable via drag-and-drop.

## View Helpers

A number of view helpers are provided for displaying banners in your templates.

`spud_banners_for_set(set_or_identifier, options)`

Accepts the banner set name as a String or Symbol and returns an html template. This helper also accepts a block argument for rendering custom html.

**Options:**

- `limit`: Limit how many banners you wish to render
- `background_image`: Pass `true` in order to render the banners as divs with css background images instead of image tags. Defaults to `false`. **This is required when rendering banners with rich text.**

```ruby
spud_banner_tag(banner)
```

Accepts a banner model object and returns a banner image wrapped in a link tag. Link tag is omitted if the `link_to` property of the banner is blank.

```ruby
spud_banner_image_tag(banner)
```

Accepts a banner model and returns only the image tag, no link.

## Examples

Displaying banners using the standard helper.

```erb
	<div id="banners">
		<%= spud_banners_for_set(:promotions) %>
	</div>
```

Displaying banners using the helper, with a block for custom html.

```erb
	<ul id="slides">
		<% spud_banners_for_set(:promotions) do |banner| %>
			<li class="custom_slide">
				<%= spud_banner_tag(banner) %>
			</li>
		<% end %>
	</ul>
```

Displaying banners using the helper, with a block for even more custom html.

```erb
	<ul id="slides">
		<% spud_banners_for_set(:promotions) do |banner| %>
			<li class="custom_slide">
				<h3><%= link_to banner.link_to, banner.title, :target => banner.link_target %></h3>
				<%= image_tag(banner.banner.url(:banner), :alt => banner.alt, :title => banner.title) %>
			</li>
		<% end %>
	</ul>
```

## Rich Text

Banner Sets contain a configuration for rich text. Turning this feature on will enable a rich text editor on individual banners. The default setting for this option is off.

Rich text is intended to be more of an edge case feature, and should be used only if your banner needs to include an editable HTML block. The built-in helpers described earlier will not automatically include your rich text; If you want to use this feature, you should supply a block to the helper and render the rich text yourself.

```erb
	<ul id="slides">
		<% spud_banners_for_set(:promotions) do |banner| %>
			<li class="custom_slide">
				<%= spud_banner_tag(banner) %>
				<div class="custom_slide_rich_text">
					<%= raw(banner.rich_text) %>
				</div>
			</li>
		<% end %>
	</ul>
```

## Slideshows

TB Banners does not provide a built-in slideshow animation library. Instead, we make it easy for you to integrate into any number of popular JavaScript slideshow plugins available on the web, or even to write your own from scratch.

Below is an example of integration with [SlidesJs][slidesjs], a jQuery plugin.

```erb
	<style type="text/css" media="screen">
		.spud_banner_set {
			width: 600px;
			height: 200px;
		}
		.spud_banner_set_banner {
			width: 600px;
			height: 200px;
			display: block;
		}
	</style>
	<div id="banners">
		<%= spud_banners_for_set(:promotions) %>
	</div>
	<script src="/slides.jquery.js" type="text/javascript"></script>
	<script>
		$(document).ready(function(){
			$("#banners").slidesjs({
				play: 5000,
				container: 'spud_banner_set'
			});
		});
	</script>
```

[tb]:https://bitbucket.org/westlakedesign/tb_core
[slidesjs]:http://www.slidesjs.com
