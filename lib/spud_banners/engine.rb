require 'tb_core'
require 'paperclip'

module Spud
  module Banners
    class Engine < Rails::Engine
      engine_name :tb_banners

      initializer :admin do
        TbCore.config.admin_applications += [{
          name: 'Banner Sets',
          thumbnail: 'admin/banners/banners.png',
          retina: true,
          url: '/admin/banner_sets',
          order: 120
        }]
      end

      initializer 'tb_banners.assets' do
        TbCore.append_admin_stylesheets('admin/banners/application')
        TbCore.append_admin_javascripts('admin/banners/application')
        Rails.application.config.assets.precompile += ['admin/banners/banners.png']
      end

    end
  end
end
