# Change Log

## v1.3.0

- Rails 5 compatibility
- Drop support for legacy iframe uploads

## v1.2.0

- Compatibility with Rails 4.2, TB Core 1.3, and Bootstrap 3.

## v1.1.2

- Improved admin banner UI, especially when working with banners that are not uniform sizes
- Add a `:background_image` option to the view helper, for users who want to display their banners as the background on a div
- Misc fixes

## v1.1.1

- Fix problem with the latest version of paperclip

## v1.1

- Support for Rails 4 and tb_core 1.2
- Introduces fragment caching for `spud_banners_for_set` helper methods

## v1.0.2

- Updated to new admin namespace
