require 'rails_helper'

describe SpudBanner, type: 'model' do

  let(:set) { create(:spud_banner_set) }

  describe 'active' do
    it 'returns active banners' do
      banner = create(:spud_banner,
        spud_banner_set_id: set.id,
        start_date: Date.yesterday, end_date: Date.tomorrow)

      result = SpudBanner.active
      expect(result).to eq([banner])
    end

    it 'does not return inactive banners' do
      create(:spud_banner,
        spud_banner_set_id: set.id,
        start_date: nil, end_date: Date.yesterday)

      result = SpudBanner.active
      expect(result).to eq([])
    end
  end

end
