require 'rails_helper'

describe Admin::BannerSetsController, type: 'controller' do

  before(:each) do
    activate_authlogic
    u = SpudUser.new(login: 'testuser', email: 'test@testuser.com', password: 'test', password_confirmation: 'test')
    u.super_admin = true
    u.save
    @user = SpudUserSession.create(u)
  end

  describe 'index' do
    it 'should return an array of banner sets' do
      5.times do |_x|
        FactoryBot.create(:spud_banner_set)
      end
      get :index
      expect(assigns(:banner_sets).count).to be > 1
    end
  end

  describe 'show' do
    it 'should respond with a banner set' do
      banner_set = FactoryBot.create(:spud_banner_set)
      get :show, params: { id: banner_set.id }
      expect(assigns(:banner_set)).to eq(banner_set)
      expect(response).to be_success
    end

    it 'should return a 404 if banner set is not found' do
      get :show, params: { id: -1 }
      expect(assigns(:banner_set)).to be_blank
      expect(response.status).to eq(404)
    end
  end

  describe 'new' do
    it 'should respond with a banner set' do
      get :new
      expect(response).to be_success
    end
  end

  describe 'create' do
    it 'should respond with a banner set' do
      expect do
        post :create, params: { spud_banner_set: FactoryBot.attributes_for(:spud_banner_set) }
      end.to change(SpudBannerSet, :count).by(1)
      expect(response).to be_success
    end

    it 'should respond unsuccessfully if the banner set is invalid' do
      expect do
        post :create, params: { spud_banner_set: { name: '', width: 'lorem', height: 'ipsum' } }
      end.to_not change(SpudBannerSet, :count)
      assert_response 422
    end
  end

  describe 'edit' do
    it 'should respond with a banner set' do
      banner_set = FactoryBot.create(:spud_banner_set)
      get :edit, params: { id: banner_set.id }
      expect(assigns(:banner_set)).to eq(banner_set)
      expect(response).to be_success
    end
  end

  describe 'update' do
    it 'should update the banner set' do
      banner_set = FactoryBot.create(:spud_banner_set)
      new_name = 'Updated Set Name'
      expect do
        put :update, params: { id: banner_set.id, spud_banner_set: { name: new_name } }
        banner_set.reload
      end.to change(banner_set, :name).to(new_name)
    end

    it 'should respond unsuccessfully if the updated banner set is invalid' do
      banner_set = FactoryBot.create(:spud_banner_set)
      expect do
        put :update, params: { id: banner_set.id, spud_banner_set: { name: '', width: 'lorem', height: 'ipsum' } }
      end.to_not change(SpudBannerSet, :count)
      assert_response 422
    end
  end

  describe 'destroy' do
    it 'should destroy the banner set and respond successfully' do
      banner_set = FactoryBot.create(:spud_banner_set)
      expect do
        delete :destroy, params: { id: banner_set.id }
      end.to change(SpudBannerSet, :count).by(-1)
      expect(response).to be_success
    end
  end

end
