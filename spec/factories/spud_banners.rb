FactoryBot.define do
  factory :spud_banner do
    banner_file_name 'test.jpg'
    banner_content_type 'image/jpeg'
    banner_file_size 1
  end
end
