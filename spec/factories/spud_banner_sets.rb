FactoryBot.define do
  factory :spud_banner_set do
    sequence :name do |i|
      "Test #{i}"
    end
    width 800
    height 200
  end
end
